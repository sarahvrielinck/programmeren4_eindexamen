@extends('layouts.master')
@section('title', 'Admin')

@section('content')
    <div id="admin">
        <div onclick="location.href='{{ action('ProductController@index') }}'">
            <a href="{{ action('ProductController@index') }}">Product</a>
        </div>
        <div onclick="location.href='http://p4ko-mikmak-kenoste.c9users.io/supplier'">
            <a href="http://p4ko-mikmak-kenoste.c9users.io/supplier">Supplier</a>
        </div>
        <div onclick="location.href='{{ action('CategoryController@index') }}'">
            <a href="{{ action('CategoryController@index') }}">Categories</a>
        </div>
        
        <div onclick="location.href='https://p4ko-mikmak-kenoste.c9users.io/customer'">
            <a href="https://p4ko-mikmak-kenoste.c9users.io/customer">Customer</a>
        </div>
        <div>
            
        </div>
        <div>
            <p>Order (n/a)</p>
        </div>
        <div>
            <p>Order Items (n/a)</p>
        </div>
        <div>
            
        </div>
        <div onclick="location.href='https://p4ko-mikmak-kenoste.c9users.io/country'">
            <a href="https://p4ko-mikmak-kenoste.c9users.io/country">Country</a>
        </div>
        <div onclick="location.href='{{ action('OrderStatusController@index')}}'">
            <a href="{{ action('OrderStatusController@index')}}">Order Status</a>
        </div>
        <div onclick="location.href='{{ action('UnitBaseController@index')}}'">
            <a href="{{ action('UnitBaseController@index')}}">Unit Base</a>
        </div>
    </div>
@endsection