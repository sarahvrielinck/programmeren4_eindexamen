@extends('layouts.master')
@section('title', 'Categories')

@section('content')

    <div class="col-lg-8">
        <div class="col-lg-12 row">
                <h2 style="float:left;">View Categories</h2>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CategoryController@create')  }}">Insert</a>
        </div>
    </div>
        
    <div class="col-md-4">
        <h3>Categories</h3>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Description</th>
                </thead>
                <tbody>
                    @foreach ($categories as $cat)
                        <tr>
                            <td><a href="{{ action('CategoryController@show', $cat) }}">Select</a></td>
                            <td>{{ $cat->name }}</td>
                            <td>{{ $cat->description }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>

@endsection