@extends('layouts.master')
@section('title', 'Create Category')

@section('content')

<script>
/*
    var categoryValidator = new (function (){
        this.name;
        this.summary;
        this.errors = [];
        this.fetchValues = function () {
            this.name = document.forms['newCategory']['name'].value;
            this.summary = document.forms['newCategory']['description'].value;
        };
        this.validate = function (){
            if (this.name.length > 255) {
                this.errors.push("Name can only contain 255 characters");
            }
            if (this.name.length == "") {
                this.errors.push("Name is required");
            }
            if (this.summary.length > 1024) {
                this.errors.push("Name can only contain 1024 characters");
            }
            if (this.errors.length > 0) {
                return false;
            }
        };
        
        
        this.fetchValues();
        this.validate();
    });
*/
</script>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form name="newCategory" method="post" onsubmit="return validate()" action="{{ action('CategoryController@store') }}">
            <h2 style="float:left;">Create Category</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning" href="{{ action('CategoryController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}
    
            <div class="form-group" style="clear:both;">
                <label for="name">Name<span style="color: red">*</span></label>
                <input class="form-control" type="text" id="name" name="name" 
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters, numbers and or french letters."
                    value="{{ old('name') }}"
                />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ old('description') }}" />
            </div>
            
        </form>
    </div>
</div>

<div class="col-lg-4">
    <h3>Categories</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($categories as $cat)
                    <tr>
                        <td><a href="{{ action('CategoryController@show', $cat) }}">Select</a></td>
                        <td>{{ $cat->name }}</td>
                        <td>{{ $cat->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $categories->links() }}
    </div>
</div>

@endsection