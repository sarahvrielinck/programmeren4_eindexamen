@extends('layouts.master')
@section('title', 'Show Category')

@section('content')
<script>
    var confirmDelete = function (){
        return confirm("Are you sure you want to delete this?");
    };
</script>

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;" class="hidden-xs">View Category</h2>
            <form method="POST" onSubmit="return confirmDelete()" action="{{ action('CategoryController@destroy', $category->id) }}">
                <input type="hidden" name="_method" value="DELETE"/>    
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('CategoryController@edit', $category)  }}">Edit</a>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CategoryController@create')  }}">Insert</a>
                <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Category</button>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CategoryController@index')  }}">Cancel</a>
            </form>
    
            <div class="form-group" style="clear:both">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $category->name }}" disabled/>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ $category->description }}" disabled/>
            </div>
    </div>
</div>

<div class="col-lg-4">
    <h3>Categories</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($categories as $cat)
                    <tr>
                        <td><a href="{{ action('CategoryController@show', $cat) }}">Select</a></td>
                        <td>{{ $cat->name }}</td>
                        <td>{{ $cat->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $categories->links() }}
    </div>
</div>
@endsection