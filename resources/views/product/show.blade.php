@extends('layouts.master')
@section('title', 'Show Product')
@section('content')

<script>
    var confirmDelete = function (){
        return confirm("Are you sure you want to delete this?");
    };
</script>

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <h2 style="float: left;" class="hidden-xs">View Product</h2>
        <form method="post" onSubmit="return confirmDelete()" action="{{ action('ProductController@destroy', $product->id) }}">
            <input type="hidden" name="_method" value="DELETE" />
            {{ csrf_field() }}
            <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('ProductController@edit', $product) }}">Edit</a>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('ProductController@create') }}">Insert</a>
            <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Product</button>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('ProductController@index') }}">Cancel</a>
        </form>

        <div class="form-group" style="clear:both;">
            <label for="name">Name</label>
            <input class="form-control" type="text" id="name" name="name" value="{{$product->name}}" disabled/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <input class="form-control" type="text" id="description" name="description" value="{{$product->description}}" disabled/>
        </div>

        <div clas="form-group">
            <label for="discountpercentage">Discount percentage</label>
            <input class="form-control" type="text" id="discountpercentage" name="discountpercentage" value="{{$product->discountpercentage}}" disabled/>
        </div>

        <div clas="form-group">
            <label for="image">Img url</label>
            <input class="form-control" type="text" id="image" name="image" value="{{$product->image}}" disabled/>
        </div>

        <div clas="form-group">
            <label for="price">Price</label>
            <input class="form-control" type="text" id="price" name="price" value="{{$product->price}}" disabled/>
        </div>

        <div clas="form-group">
            <label for="shippingcost">Shipping cost</label>
            <input class="form-control" type="text" id="shippingcost" name="shippingcost" value="{{$product->shippingcost}}" disabled/>
        </div>

        <div clas="form-group">
            <label for="thumbnail">Thumbnail</label>
            <input class="form-control" type="text" id="thumbnail" name="thumbnail" value="{{$product->thumbnail}}" disabled/>
        </div>
        
        <div class="form-group">
            <label for="category">Category</label>
            <input class="form-control" type="text" id="category" name="category" value="{{ $product->category->name }}" disabled/>
        </div>
        
        <div class="form-group">
            <label for="unitbase">Unit Base</label>
            <input class="form-control" type="text" id="unitbase" name="unitbase" value="{{ $product->unitbase->code }}" disabled/>
        </div>
    </div>
</div>

<div class="col-md-4">
    <h3>Products</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td><a href="{{ action('ProductController@show', $product) }}">Select</a></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>

@endsection