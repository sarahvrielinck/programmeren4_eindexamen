@extends('layouts.master')
@section('title', 'Products')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View Products</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('ProductController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Products</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td><a href="{{ action('ProductController@show', $product) }}">Select</a></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>

@endsection