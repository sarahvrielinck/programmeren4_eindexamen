@extends('layouts.master')
@section('title', 'Edit Product')

@section('content')

<script>
 /*   var volidateForm = function (){
        var name = document.forms['newProduct']['name'].value;
        var containsErrors = false;
        if (name == ""){
            document.getElementById('nameError').innerHTML = "* name cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById('nameError').innerHTML = "";
        }
        
        if (containsErrors) {
            return false;
        }
    }; */
</script>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <h2 style="float: left;">View Product</h2>
        <form method="POST" onSubmit="return validateForm()" action="{{ action('ProductController@update', $product->id) }}">
            <input type="hidden" name="_method" value="PUT" />
            {{ csrf_field() }}
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('ProductController@index')  }}">Cancel</a>
            <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
        

            <div class="form-group" style="clear:both;">
                <label for="name">Name<span style="color: red">*</span></label>
                <input class="form-control" type="text" id="name" name="name" value="{{$product->name}}" 
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters, numbers and or french letters."
                >
                <span id="nameError"></span>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{$product->description}}" >
            </div>
    
            <div clas="form-group">
                <label for="discountpercentage">Discount percentage</label>
                <input class="form-control" type="text" id="discountpercentage" name="discountpercentage" value="{{$product->discountpercentage}}"
                    pattern="\d+(?:\.\d+)?"
                    title="Example: 10.2%"
                />
            </div>
    
            <div clas="form-group">
                <label for="image">Img url</label>
                <input class="form-control" type="text" id="image" name="image" value="{{$product->image}}"
                    pattern="(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
                    title="Example: https://someimage.be/image.jpg"
                />
            </div>
    
            <div clas="form-group">
                <label for="price">Price</label>
                <input class="form-control" type="text" id="price" name="price" value="{{$product->price}}"
                    pattern="\d{1,}(\.\d{1,2})?"
                    title="Price is represented like: 12.22. With a maximum of two decimals"
                />
            </div>
    
            <div clas="form-group">
                <label for="shippingcost">Shipping cost</label>
                <input class="form-control" type="text" id="shippingcost" name="shippingcost" value="{{$product->shippingcost}}"
                    pattern="\d{1,}(\.\d{1,2})?"
                    title="Price is represented like: 12.22. With a maximum of two decimals"
                />
            </div>
    
            <div clas="form-group">
                <label for="thumbnail">Thumbnail</label>
                <input class="form-control" type="text" id="thumbnail" name="thumbnail" value="{{$product->thumbnail}}"
                    pattern="(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
                    title="Example: https://someimage.be/image.jpg"
                />
            </div>
            
            <div class="form-group">
                <label for="category">Category</label>
                <select id="category" name="category" class="form-control">
                    @foreach($categories as $cat)
                        @if($cat->id == $product->category_id)
                            <option value="{{ $cat->id }}" class="form-control" selected>{{ $cat->name }}</option>
                        @else
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="unitbase">Unit base</label>
                <select id="unitbase" name="unitbase" class="form-control">
                    @foreach($unitbases as $unitbase)
                        @if($unitbase->id == $product->unitbase_id)
                            <option class="form-control" value="{{ $unitbase->id }}" selected>{{ $unitbase->name }}</option>
                        @else
                            <option class="form-control" value="{{ $unitbase->id }}">{{ $unitbase->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </form>
    </div>
</div>


<div class="col-md-4">
    <h3>Products</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td><a href="{{ action('ProductController@show', $product) }}">Select</a></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>

@endsection