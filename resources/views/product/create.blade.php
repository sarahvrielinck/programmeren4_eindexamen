@extends('layouts.master')
@section('title', 'Create Product')

@section('content')

<script>
/*    var volidateForm = function (){
        var name = document.forms['newProduct']['name'].value;
        var containsErrors = false;
        if (name == ""){
            document.getElementById('nameError').innerHTML = "* name cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById('nameError').innerHTML = "";
        }
        
        if (containsErrors) {
            return false;
        }
    }; */
</script>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form name="newProduct" method="post" onSubmit="return validateForm()" action="{{ action('ProductController@store') }}">
            <h2 style="float:left">Create Product</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning"href="{{ action('ProductController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}

            <div class="form-group" style="clear:both;">
                <label for="name">Name<span style="color: red">*</span></label>
                <input class="form-control" type="text" id="name" name="name"
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters, numbers and or french letters."
                    value="{{ old('name') }}"
                />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" id="description" name="description" type="text" value="{{ old('description') }}"/>
            </div>
            
            <div clas="form-group">
                <label for="discountpercentage">Discount percentage</label>
                <input class="form-control" type="text" id="discountpercentage" name="discountpercentage"
                    pattern="\d+(?:\.\d{1,2})?"
                    title="Example: 10.2"
                    value="{{ old('discountpercentage') }}"
                />
            </div>
            
            <div clas="form-group">
                <label for="image">Img url</label>
                <input class="form-control" type="text" id="image" name="image"
                    pattern="(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
                    title="Example: https://someimage.be/image.jpg"
                    value="{{ old('image') }}"
                />
            </div>
            
            <div clas="form-group">
                <label for="price">Price</label>
                <input class="form-control" type="text" id="price" name="price"
                    pattern="\d{1,}(\.\d{1,2})?"
                    title="Price is represented like: 12.22. With a maximum of two decimals"
                    value="{{ old('price') }}"
                />
            </div>
            
            <div clas="form-group">
                <label for="shippingcost">Shipping cost</label>
                <input class="form-control" type="text" id="shippingcost" name="shippingcost"
                    pattern="\d{1,}(\.\d{1,2})?"
                    title="Price is represented like: 12.22. With a maximum of two decimals"
                    value="{{ old('shippingcost') }}"
                />
            </div>
            
            <div clas="form-group">
                <label for="thumbnail">Thumbnail</label>
                <input class="form-control" type="text" id="thumbnail" name="thumbnail"
                    pattern="(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
                    title="Example: https://someimage.be/image.jpg"
                    value="{{ old('thumbnail') }}"
                />
            </div>
            
            <div class="form-group">
                <label for="category">Category</label>
                <select id="category" name="category" class="form-control">
                    @foreach($categories as $cat)
                        <option value="{{ $cat->id }}" class="form-control">{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="unitbase">Unit base</label>
                <select id="unitbase" name="unitbase" class="form-control">
                    @foreach($unitbases as $unitbase)
                        <option value="{{ $unitbase->id }}" class="form-control">{{ $unitbase->name }}</option>
                    @endforeach
                </select>
            </div>
        </form>
    </div>
</div>

<div class="col-lg-4">
    <h3>Products</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td><a href="{{ action('ProductController@show', $product) }}">Select</a></td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>
@endsection