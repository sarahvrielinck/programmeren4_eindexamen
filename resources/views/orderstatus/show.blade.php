@extends('layouts.master')
@section('title', 'Show Order Status')

@section('content')
<script>
    var confirmDelete = function (){
        return confirm("Are you sure you want to delete this?");
    };
</script>

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;" class="hidden-xs">View order status</h2>
            <form method="POST" onSubmit="return confirmDelete()" action="{{ action('OrderStatusController@destroy', $orderstatus->id) }}">
                <input type="hidden" name="_method" value="DELETE"/>    
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('OrderStatusController@edit', $orderstatus)  }}">Edit</a>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('OrderStatusController@create')  }}">Insert</a>
                <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Category</button>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('OrderStatusController@index')  }}">Cancel</a>
            </form>
    
            <div class="form-group" style="clear:both">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="{{ $orderstatus->name }}" disabled/>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ $orderstatus->description }}" disabled/>
            </div>
    </div>
</div>

<div class="col-md-4">
    <h3>Order statuses</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($orderstatuses as $status)
                    <tr>
                        <td><a href="{{ action('OrderStatusController@show', $status) }}">Select</a></td>
                        <td>{{ $status->name }}</td>
                        <td>{{ $status->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orderstatuses->links() }}
    </div>
</div>
@endsection