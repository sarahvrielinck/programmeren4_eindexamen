@extends('layouts.master')
@section('title', 'Edit Order Status')

@section('content')
    
<script>
/*    var validateForm = function () {
        var name = document.forms["editCategory"]["name"].value;
        var containsErrors = false;
        if (name == "") {
            document.getElementById ("nameError").innerHTML = "* name cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById("nameError").innerHTML = "";
        }
        if (containsErrors) {
            return false;
        }
    };*/
</script>
   
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    
<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">Edit Order Status</h2>
            <form name="editOrderStatus" onsubmit="return validateForm()" method="POST" action="{{ action('OrderStatusController@update', $orderstatus->id) }}">
                <input type="hidden" name="_method" value="PUT"/>  
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('OrderStatusController@index')  }}">Cancel</a>
                <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
                
            <div class="form-group" style="clear:both;">
                <label for="name">name<span style="color: red">*</span></label> 
                <input type="text" name="name" class="form-control" id="name" value="{{ $orderstatus->name }}"
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters or numbers (,-. are allowed)."
                />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
            </div>
                
            <div class="form-group">
                <label for="description">description</label> 
                <textarea name="description" class="form-control" id="description">{{ $orderstatus->description }}</textarea>
            </div>
            </form>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Order statuses</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($orderstatuses as $status)
                    <tr>
                        <td><a href="{{ action('OrderStatusController@show', $status) }}">Select</a></td>
                        <td>{{ $status->name }}</td>
                        <td>{{ $status->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orderstatuses->links() }}
    </div>
</div>

@endsection