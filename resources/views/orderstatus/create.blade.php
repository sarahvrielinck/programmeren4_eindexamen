@extends('layouts.master')
@section('title', 'Create Order Status')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form name="newOrderStatus" method="post" onsubmit="return validate()" action="{{ action('OrderStatusController@store') }}">
            <h2 style="float:left;">Create order status</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning" href="{{ action('OrderStatusController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}
    
            <div class="form-group" style="clear:both;">
                <label for="name">Name<span style="color: red">*</span></label>
                <input class="form-control" type="text" id="name" name="name" 
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters, numbers and or french letters."
                    value="{{ old('name') }}"
                />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ old('description') }}" />
            </div>
            
        </form>
    </div>
</div>

<div class="col-md-4">
    <h3>Order statuses</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($orderstatuses as $status)
                    <tr>
                        <td><a href="{{ action('OrderStatusController@show', $status) }}">Select</a></td>
                        <td>{{ $status->name }}</td>
                        <td>{{ $status->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orderstatuses->links() }}
    </div>
</div>

@endsection