@extends('layouts.master')
@section('title', 'Index Order Status')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View order statuses</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('OrderStatusController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Order statuses</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Description</th>
            </thead>
            <tbody>
                @foreach ($orderstatuses as $status)
                    <tr>
                        <td><a href="{{ action('OrderStatusController@show', $status) }}">Select</a></td>
                        <td>{{ $status->name }}</td>
                        <td>{{ $status->description }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orderstatuses->links() }}
    </div>
</div>

@endsection