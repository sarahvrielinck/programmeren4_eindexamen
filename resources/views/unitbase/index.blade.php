@extends('layouts.master')
@section('title', 'Unit Bases')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View Unit Bases</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('UnitBaseController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Unit Bases</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($unitBases as $unitBase)
                    <tr>
                        <td><a href="{{ action('UnitBaseController@show', $unitBase) }}">Select</a></td>
                        <td>{{ $unitBase->name }}</td>
                        <td>{{ $unitBase->code }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $unitBases->links() }}
    </div>
</div>

@endsection