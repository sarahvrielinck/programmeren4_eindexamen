@extends('layouts.master')
@section('title', 'Create unit base')

@section('content')

<script>
/*    var validateForm = function () {
        var name = document.forms["newUnitBase"]["name"].value;
        var containsErrors = false;
        if (name == "") {
            document.getElementById ("nameError").innerHTML = "* name cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById("nameError").innerHTML = "";
        }
        var code = document.forms['newUnitBase']['code'].value;
        if (code == "") {
            document.getElementById("codeError").innerHTML = "* code cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById("codeError").innerHTML = "";
        }
        
        if (containsErrors) {
            return false;
        }
    }; */
</script>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form name="newUnitBase" method="post" onsubmit="return validateForm()" action="{{ action('UnitBaseController@store') }}">
            <h2 style="float:left;">Create Unit Base</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning" href="{{ action('UnitBaseController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}
    
            <div class="form-group" style="clear:both;">
                <label for="name">Name<span style="color: red">*</span></label>
                <input class="form-control" type="text" id="name" name="name" 
                    pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                    title="Name should only contain lower or uppercase letters, numbers and or french letters."
                    value="{{ old('name') }}"
                />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
            </div>
    
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" type="text" id="description" name="description" value="{{ old('description') }}"/>
            </div>
            
            <div class="form-group">
                <label for="code">Code</label>
                <input class="form-control" type="text" id="code" name="code" 
                    pattern="[a-zA-Z0-9]{1,2}"
                    title="Code can only contain upper or lowercase character and numbers. With a maximum of 2."
                    value="{{ old('code') }}"
                />
                <span id="codeError" style="display:block; color:red; font-style:italic;"></span>
            </div>
            
            <div class="form-group">
                <label for="shippingCostMultiplier">Shippingcost Multiplier</label>
                <input class="form-control" type="text" id="shippingCostMultiplier" name="shippingCostMultiplier" 
                    pattern="\d+(?:\.\d{1,2})?"
                    title="Shippingcost Multiplier can only be a decimal value with a precission of two. Ex. 12.22"
                    value="{{ old('shippingCostMultiplier') }}"
                />
            </div>
            
        </form>
    </div>
</div>

<div class="col-md-4">
    <h3>Unit Bases</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($unitBases as $unitBase)
                    <tr>
                        <td><a href="{{ action('UnitBaseController@show', $unitBase) }}">Select</a></td>
                        <td>{{ $unitBase->name }}</td>
                        <td>{{ $unitBase->code }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $unitBases->links() }}
    </div>
</div>
@endsection