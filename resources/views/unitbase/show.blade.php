@extends('layouts.master')
@section('title', 'Show Unit Base')

@section('content')
<script>
    var confirmDelete = function (){
        return confirm("Are you sure you want to delete this?");
    };
</script>

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <h2 style="float:left;" class="hidden-xs">View UnitBase</h2>
        <form method="POST" onSubmit="return confirmDelete()" action="{{ action('UnitBaseController@destroy', $unitBase->id) }}">
            <input type="hidden" name="_method" value="DELETE"/>    
            {{ csrf_field() }}
            <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('UnitBaseController@edit', $unitBase)  }}">Edit</a>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('UnitBaseController@create')  }}">Insert</a>
            <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete UnitBase</button>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('UnitBaseController@index')  }}">Cancel</a>
        </form>

        <div class="form-group" style="clear:both">
            <label for="name">Name</label>
            <input class="form-control" type="text" id="name" name="name" value="{{ $unitBase->name }}" disabled/>
        </div>
    
        <div class="form-group">
            <label for="description">Description</label>
            <input class="form-control" type="text" id="description" name="description" value="{{ $unitBase->description }}" disabled/>
        </div>
            
        <div class="form-group">
            <label for="code">Code</label>
            <input class="form-control" type="text" id="code" name="code" value="{{ $unitBase->code }}" disabled/>
        </div>
            
        <div class="form-group">
            <label for="shippingCost">Shipping Cost Multiplier</label>
            <input class="form-control" type="text" id="shippingCost" name="shippingCostMultiplier" value="{{ $unitBase->shippingcostmultiplier }}" disabled/>
        </div>
    </div>
</div>

<div class="col-md-4">
    <h3>Unit Bases</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($unitBases as $unitBase)
                    <tr>
                        <td><a href="{{ action('UnitBaseController@show', $unitBase) }}">Select</a></td>
                        <td>{{ $unitBase->name }}</td>
                        <td>{{ $unitBase->code }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $unitBases->links() }}
    </div>
</div>
@endsection