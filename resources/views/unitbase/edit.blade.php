@extends('layouts.master')
@section('title', 'Edit Unit Base')

@section('content')
    
<script>
/*    var validateForm = function () {
        var name = document.forms["editUnitBase"]["name"].value;
        var containsErrors = false;
        if (name == "") {
            document.getElementById ("nameError").innerHTML = "* name cannot be empty";
            containsErrors = true;
        } else {
            document.getElementById("nameError").innerHTML = "";
        }
        if (containsErrors) {
            return false;
        }
    }; */
</script>
    
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul style="margin-top: 5px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    
<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">Edit Unit Base</h2>
            <form name="editUnitBase" onsubmit="return validateForm()" method="POST" action="{{ action('UnitBaseController@update', $unitBase->id) }}">
                <input type="hidden" name="_method" value="PUT"/>  
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('UnitBaseController@index')  }}">Cancel</a>
                <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
                
                <div class="form-group" style="clear:both;">
                    <label for="name">Name<span style="color: red">*</span></label>
                    <input class="form-control" type="text" id="name" name="name" 
                        pattern="[a-zA-Z0-9\s,-.àâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇ]+"
                        title="Name should only contain lower or uppercase letters, numbers and or french letters."
                        value="{{ $unitBase->name }}"
                    />
                <span id="nameError" style="display:block; color:red; font-style:italic;"></span>
                </div>
        
                <div class="form-group">
                    <label for="description">Description</label>
                    <input class="form-control" type="text" id="description" name="description"
                        value="{{ $unitBase->description }}"
                    />
                </div>
                
                <div class="form-group">
                    <label for="code">Code</label>
                    <input class="form-control" type="text" id="code" name="code" 
                        pattern="[a-zA-Z0-9]{1,2}"
                        title="Code can only contain upper or lowercase character and numbers. With a maximum of 2."
                        value="{{ $unitBase->code }}"
                    />
                </div>
                
                <div class="form-group">
                    <label for="shippingCostMultiplier">Shippingcost Multiplier</label>
                    <input class="form-control" type="text" id="shippingCostMultiplier" name="shippingCostMultiplier" 
                        pattern="\d+(?:\.\d{1,2})?"
                        title="Shippingcost Multiplier can only be a decimal value with a precission of two. Ex. 12.22"
                        value="{{ $unitBase->shippingcostmultiplier }}"
                    />
                </div>
            </form>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Unit Bases</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($unitBases as $unitBase)
                    <tr>
                        <td><a href="{{ action('UnitBaseController@show', $unitBase) }}">Select</a></td>
                        <td>{{ $unitBase->name }}</td>
                        <td>{{ $unitBase->code }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $unitBases->links() }}
    </div>
</div>
@endsection