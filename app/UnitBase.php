<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitBase extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
        'shippingcostmultiplier',
        'code',
    ];
}
