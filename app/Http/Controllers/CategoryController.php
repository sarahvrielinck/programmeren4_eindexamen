<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class CategoryController extends Controller
{
    public function __construct() 
    {
        setlocale(LC_TIME, 'nl-BE');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = Category::orderBy('name', 'asc')->paginate(5);
        return view('category.index', ['categories' => $cats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::orderBy('name', 'asc')->paginate(5);
        return view('category.create', ['categories' => $cats]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name|max:255',
            'description' => 'max:1024'
        ]); // will jump out of function if validation fails
        
        $cat = new Category;
        $cat->name = $request->input('name');
        $cat->description = $request->input('description');
        
        $cat->save();

        return redirect('/Category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat = Category::find($id);
        $cats = Category::orderBy('name', 'asc')->paginate(5);
        return view('category.show', ['category' => $cat, 'categories' => $cats]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);
        $cats = Category::orderBy('name', 'asc')->paginate(5);
        return view('category.edit', ['category' => $cat, 'categories' => $cats]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id == 1){
            return redirect('/Category');
        }
        
        $cat = Category::find($id);
        
        if ($request->input('name') == $cat->name) {
            $this->validate($request, [
                'description' => 'max:1024',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|unique:categories,name|max:255',
                'description' => 'max:1024'
            ]); // will jump out of function if validation fails
        }
        
        $cat = Category::find($id);
        $cat->name = $request->input('name');
        $cat->description = $request->input('description');
        $cat->save();
        
        return redirect('/Category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == 1){
            return redirect('/Category');
        } else {
             \DB::table('products')
            ->where('category_id', $id)
            ->update(['category_id' => 1]);
            
            $cat = Category::find($id);
            $cat->delete();
            
            return redirect('/Category');
        }
    }
}
