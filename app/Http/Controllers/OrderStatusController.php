<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderStatus;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderStatuses = OrderStatus::orderBy('name', 'asc')->paginate(5);
        return view('orderstatus.index', ['orderstatuses' => $orderStatuses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orderStatuses = OrderStatus::orderBy('name', 'asc')->paginate(5);
        return view('orderstatus.create', ['orderstatuses' => $orderStatuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:order_statuses,name|max:255',
            'description' => 'max:1024'
        ]); // will jump out of function if validation fails
        
        $status = new OrderStatus;
        $status->name = $request->input('name');
        $status->description = $request->input('description');
        
        $status->save();

        return redirect('/OrderStatus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderStatus = OrderStatus::find($id);
        $orderStatuses = OrderStatus::orderBy('name', 'asc')->paginate(5);
        return view('orderstatus.show', ['orderstatus' => $orderStatus, 'orderstatuses' => $orderStatuses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderStatus = OrderStatus::find($id);
        $orderStatuses = OrderStatus::orderBy('name', 'asc')->paginate(5);
        return view('orderstatus.edit', ['orderstatus' => $orderStatus, 'orderstatuses' => $orderStatuses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id == 1){
            return redirect('/OrderStatus');
        }
        
        $status = OrderStatus::find($id);
        
        if ($request->input('name') == $status->name) {
            $this->validate($request, [
                'description' => 'max:1024',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|unique:order_statuses,name|max:255',
                'description' => 'max:1024'
            ]); // will jump out of function if validation fails
        }
        
        $status = OrderStatus::find($id);
        $status->name = $request->input('name');
        $status->description = $request->input('description');
        $status->save();
        
        return redirect('/OrderStatus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == 1){
            return redirect('/OrderStatus');
        } else {
            // search & destroy
            $orderStatus = OrderStatus::find($id);
            $orderStatus->delete();
            
            return redirect('/OrderStatus');
        }
    }
}
