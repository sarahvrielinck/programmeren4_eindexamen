<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\UnitBase;

class ProductController extends Controller
{
    // ToDo: Validatie schrijven
    public function __construct() 
    {
        setlocale(LC_TIME, 'nl-BE');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('name', 'asc')->paginate(5);
        return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::orderBy('name', 'asc')->get();
        $products = Product::orderBy('name', 'asc')->Paginate(5);
        $unitbases = UnitBase::orderBy('name', 'asc')->get();
        return view('product.create', ['categories' => $cats, 'products' => $products, 'unitbases' => $unitbases]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'description' => 'max:1024',
            'name' => 'required|unique:products,name|max:255',
            'price' => 'regex:/^-?\d*(\.\d+)?$/',
            'shippingcost' => 'regex:/^-?\d*(\.\d+)?$/',
            'discountpercentage' => 'regex:/^-?\d*(\.\d+)?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
        ]); // will jump out of function if validation fails
        
        $product = new Product;
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->discountpercentage = $request->input('discountpercentage');
        $product->image = $request->input('image');
        $product->price = $request->input('price');
        $product->shippingcost = $request->input('shippingcost');
        $product->thumbnail = $request->input('thumbnail');
        $product->category_id = $request->input('category');
        $product->unitbase_id = $request->input('unitbase');
        $product->save();
        
        return redirect('/Product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $products = Product::orderBy('name', 'asc')->paginate(5);
        return view("product.show", ['product' => $product, 'products' => $products]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $cats = Category::orderBy('name', 'asc')->get();
        $products = Product::orderBy('name', 'asc')->paginate(5);
        $unitbases = UnitBase::orderBy('name', 'asc')->get();
        return view('product.edit', ['product' => $product, 'categories' => $cats, 'products' => $products, 'unitbases' => $unitbases]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $nameValidateBuilder;
        
        if ($request->input('name') == $product->name) {
            $nameValidateBuilder = 'required|max:255';
        } else {
            $nameValidateBuilder = "required|unique:products,name|max:255";
        }
        
        
        $this->validate($request, [
            'description' => 'max:1024',
            'name' => $nameValidateBuilder,
            'price' => 'regex:/^-?\d*(\.\d+)?$/',
            'shippingcost' => 'regex:/^-?\d*(\.\d+)?$/',
            'discountpercentage' => 'regex:/^-?\d*(\.\d+)?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
        ]); // will jump out of function if validation fails
        
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->discountpercentage = $request->input('discountpercentage');
        $product->image = $request->input('image');
        $product->price = $request->input('price');
        $product->shippingcost = $request->input('shippingcost');
        $product->thumbnail = $request->input('thumbnail');
        $product->category_id = $request->input('category');
        $product->unitbase_id = $request->input('unitbase');
        $product->save();
        
        
        
        return redirect('/Product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete(); 
        
        return redirect('/Product');
    }
}