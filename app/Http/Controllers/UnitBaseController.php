<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnitBase;

class UnitBaseController extends Controller
{
    public function __construct() 
    {
        setlocale(LC_TIME, 'nl-BE');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unitBases = UnitBase::orderBy('name', 'asc')->paginate(5);
        return view ('unitbase.index', ['unitBases' => $unitBases]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unitBases = UnitBase::orderBy('name', 'asc')->paginate(5);
        return view ('unitbase.create', ['unitBases' => $unitBases]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:unit_bases,name|max:255',
            'description' => 'max:1024',
            'shippingcostmultiplier' => 'regex:/^-?\d*(\.\d+)?$/',
            'code' => 'required|unique:unit_bases,code|max:2',
        ]); // will jump out of function if validation fails
        
        
        $unitBase = new UnitBase;
        $unitBase->name = $request->input('name');
        $unitBase->description = $request->input('description');
        $unitBase->code = $request->input('code');
        $unitBase->shippingcostmultiplier = $request->input('shippingCostMultiplier');
        $unitBase->save();
        
        return redirect('/UnitBase');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unitBase = UnitBase::find($id);
        $ubs = UnitBase::orderBy('name', 'asc')->paginate(5);
        return view('unitbase.show', ['unitBase' => $unitBase, 'unitBases' => $ubs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unitBase = UnitBase::find($id);
        $ubs = UnitBase::orderBy('name', 'asc')->paginate(5);
        return view('unitbase.edit', ['unitBase' => $unitBase, 'unitBases' => $ubs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id == 1){
            return redirect('/UnitBase');
        }
        
        $unitBase = UnitBase::find($id);
        
        $nameValidateBuilder;
        $codeValidateBuilder;
        
        if ($request->input('name') == $unitBase->name)
        {
            $nameValidateBuilder = 'required|max:255';
        } else {
            $nameValidateBuilder = 'required|unique:unit_bases,name|max:255';
        }
        
        if ($request->input('code') == $unitBase->code){
            $codeValidateBuilder = 'required|max:2';
        } else {
            $codeValidateBuilder = 'required|unique:unit_bases,code|max:2';
        }
        
        $this->validate($request, [
            'name' => $nameValidateBuilder,
            'description' => 'max:1024',
            'code' => $codeValidateBuilder,
            'shippingcostmultiplier' => 'regex:/^-?\d*(\.\d+)?$/',
        ]); // will jump out of function if validation fails
        
        $unitBase->name = $request->input('name');
        $unitBase->description = $request->input('description');
        $unitBase->code = $request->input('code');
        $unitBase->shippingcostmultiplier = $request->input('shippingCostMultiplier');
        $unitBase->save();
        
        return redirect('/UnitBase');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == 1){
            return redirect('/UnitBase');
        } else {
             \DB::table('products')
            ->where('unitbase_id', $id)
            ->update(['unitbase_id' => 1]);
            $unitBase = UnitBase::find($id);
            $unitBase->delete();
            
            return redirect('/UnitBase');
        }
    }
}
