<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
}); */

Route::resource('/', 'HomeController');
Route::resource('Category', 'CategoryController');
Route::resource('Product', 'ProductController');
Route::resource('Admin', 'AdminController');
Route::resource('UnitBase', 'UnitBaseController');
Route::resource('OrderStatus', 'OrderStatusController');