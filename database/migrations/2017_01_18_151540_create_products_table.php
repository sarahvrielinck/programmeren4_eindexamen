<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 1024);
            $table->string('name', 255);
            $table->float('price');
            $table->float('shippingcost');
            $table->integer('totalrating');
            $table->char('thumbnail', 255);
            $table->char('image', 255);
            $table->float('discountpercentage');
            $table->integer('votes');
            $table->timestamps();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('unitbase_id')->unsigned()->nullable();
            
        });
        
        Schema::table('products', function($table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('unitbase_id')->references('id')->on('unit_bases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
