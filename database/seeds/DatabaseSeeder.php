<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\UnitBase;
use App\OrderStatus;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoryTableSeeder::class);
        $this->call(UnitBaseSeeder::class);
        $this->call(OrderStatusSeeder::class);
    }
}

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = new Category();
        $cat->name = "unknown";
        $cat->description = "unknown";
        $cat->save();
    }
}

class UnitBaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ub = new UnitBase();
        $ub->name = "unknown";
        $ub->description = "unknown";
        $ub->code = "uk";
        $ub->save();
    }
}

class OrderStatusSeeder extends Seeder
{
    public function run ()
    {
        $orderStatus = new OrderStatus();
        $orderStatus->name = "unknown";
        $orderStatus->description = "unknown";
        $orderStatus->save();
    }
}